# Example for don

## Building

Simple version:

```bash
buildah bud -f builds/don-example .
```

Mirroring what is happening in GitLab CI:

```bash
podman run \
    --rm \
    -e IMAGE_NAME=don-example \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

## Uploading the image to :latest

This should only be done on the master branch of the repository on GitLab 😛.

## Deploying

After the image is successfully uploaded to the GitLab container image
registry, the example can be deployed via

```bash
oc login
oc apply -f deployment.yml
```

To see the deployed resources:

```bash
oc get all -l app=don-example
```

To see the shell output:

```bash
oc logs dc/don-example
```

To get a shell in the running container:

```bash
oc rsh dc/don-example /bin/bash
```
